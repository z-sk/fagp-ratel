package com.fagp.gateway;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@ComponentScan( {"com.fagp.gateway", "com.zebra.gateway"})
@Slf4j
public class GatewayApplication {
    public static void main(String[] args) {
        try{
            System.setProperty("Log4jContextSelector",   "org.apache.logging.log4j.core.async.AsyncLoggerContextSelector");
            SpringApplication.run(GatewayApplication.class, args);
        }catch (Exception e){
            log.error("start error:", e);
        }
    }
}
