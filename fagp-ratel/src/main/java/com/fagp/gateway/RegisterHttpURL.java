package com.fagp.gateway;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.zebra.gateway.GatewayUtil;
import com.zebra.remoting.config.GatewayServerInfo;
import com.zebra.remoting.config.GwRegisterResponse;

import java.io.IOException;

public class RegisterHttpURL {

    private final static String REGISTER_RUL = "/register/gw";

    public static GwRegisterResponse registerGw(GatewayServerInfo serverInfo, String raAddr) throws IOException {

        String url = "http://" + raAddr + RegisterHttpURL.REGISTER_RUL;
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
        String requestjson = gson.toJson(serverInfo);
        //-----------需要通过注册中心--------
        String json = GatewayUtil.doPost(url, requestjson);
        return gson.fromJson(json, GwRegisterResponse.class);

    }


}
