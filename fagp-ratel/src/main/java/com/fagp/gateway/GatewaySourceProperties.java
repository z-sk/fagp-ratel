package com.fagp.gateway;

import lombok.Data;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@Data
@ToString
@PropertySource(value = {"file:${user.dir}/${config.path}"}, encoding="utf-8")
public class GatewaySourceProperties {

    @Value("${zebra.ip.out}")
    private String ipOut;

    @Value("${zebra.ip.in}")
    private String ipIn;

    // 0 out;  1 in
    @Value("${zebra.wg.inner}")
    private int inner;

    //register
    @Value("${zebra.wg.register-addr}")
    private String registerAddr;
}
