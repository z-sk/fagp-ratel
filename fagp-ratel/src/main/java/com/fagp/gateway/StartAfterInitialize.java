package com.fagp.gateway;

import com.zebra.gateway.GatewayStartAfterInitialize;
import com.zebra.gateway.GwConfigInfoManager;
import com.zebra.remoting.config.GwRegisterResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @Author King.Song
 * @Date 2020/10/9 0009
 **/
@Component
@Order(2)
@Slf4j
public class StartAfterInitialize implements ApplicationRunner {

    @Autowired
    private GatewayStartAfterInitialize gatewayStartAfterInitialize;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        String raAddr = GwConfigInfoManager.getInstance().getRegisterAddr();
        GwRegisterResponse registerResponse = RegisterHttpURL.registerGw(GwConfigInfoManager.getInstance().getGwServerInfo(), raAddr);
        gatewayStartAfterInitialize.start(registerResponse.getCmdSet(), registerResponse.getTmsList(), registerResponse.getGwInfo().getServerId());
    }

}
