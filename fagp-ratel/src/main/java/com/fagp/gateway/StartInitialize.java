package com.fagp.gateway;

import com.zebra.gateway.GatewayServerApplication;
import com.zebra.gateway.SourceProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.core.async.AsyncLoggerContextSelector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
@Slf4j
public class StartInitialize implements ApplicationRunner {

    @Autowired
    private GatewayServerApplication gatewayServerApplication;
    @Autowired
    private GatewaySourceProperties gatewaySourceProperties;
    @Value("${logging.lv}")
    private String logLV;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        if (StringUtils.isLegal(logLV)){
            return;
        }

        log.info("log is async：{}", AsyncLoggerContextSelector.isSelected());
        SourceProperties properties = SourceProperties.builder()
                .ipIn(gatewaySourceProperties.getIpIn())
                .ipOut(gatewaySourceProperties.getIpOut())
                .inner(gatewaySourceProperties.getInner())
                .registerAddr(gatewaySourceProperties.getRegisterAddr())
                .build();
        gatewayServerApplication.run(properties);
    }

}
