package com.fagp.register.provide;

import com.fagp.mew.common.rest.bo.MewSession;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@Component
@FeignClient(value = "mew-web-auc")
public interface AucFeignService {

    @GetMapping(value = "/get/{uid}")
    @ResponseBody
    MewSession getSession(@PathVariable("uid") long uid);


    @GetMapping(value = "/tmsoff/{sid}/{servetype}")
    @ResponseBody
    Integer tmsoff(@PathVariable("sid") int sid, @PathVariable("servetype") int servetype);

}
