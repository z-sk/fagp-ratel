package com.fagp.register;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;


@EnableDiscoveryClient
@SpringBootApplication
@EnableFeignClients(basePackages = {"com.fagp.register.provide"})
@ComponentScan( {"com.fagp.register", "com.zebra.register"})
@Slf4j
public class RegisterApplication {
    public static void main(String[] args) {
        try{
            System.setProperty("Log4jContextSelector",   "org.apache.logging.log4j.core.async.AsyncLoggerContextSelector");
            SpringApplication.run(RegisterApplication.class, args);
        }catch (Exception e){
            log.error("start kernel user error", e);
        }
    }
}
