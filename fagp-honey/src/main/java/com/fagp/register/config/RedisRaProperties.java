package com.fagp.register.config;

import lombok.Data;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @author songk.
 * @date 16:10 2020/05/18
 */
@Configuration
@Data
@ToString
@PropertySource(value = {"file:${user.dir}/${config.path}"}, encoding="utf-8")
public class RedisRaProperties {

    @Value("${redis.database}")
    private int database;

    @Value("${redis.host}")
    private String host;

    @Value("${redis.port}")
    private int port;

    @Value("${redis.password}")
    private String password;

    private long timeout = 5000;
    private int maxActive = 16;
    private int maxIdle = 16;
    private int minIdle = 8;
    private int maxWait = 10000;

}
