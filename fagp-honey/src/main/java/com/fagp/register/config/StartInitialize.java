package com.fagp.register.config;

import com.fagp.register.handler.RegisterHandler;
import com.zebra.register.RegisterStartService;
import com.zebra.remoting.IpInfoProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;


@Component
@Order(1)
@Slf4j
public class StartInitialize implements ApplicationRunner {

    @Autowired
    private RegisterStartService registerStartService;
    @Autowired
    private RegisterHandler registerHandler;
    @Autowired
    private RegisterSourceProperties registerSourceProperties;

    @Override
    public void run(ApplicationArguments args) throws Exception {
//        log.info("log is async：{}", AsyncLoggerContextSelector.isSelected());
        IpInfoProperties properties = IpInfoProperties.builder().ipIn(registerSourceProperties.getIpIn()).ipOut(registerSourceProperties.getIpOut()).build();
        registerStartService.start(properties, registerHandler);

    }




}
