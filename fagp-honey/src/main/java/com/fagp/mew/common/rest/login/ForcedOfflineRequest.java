package com.fagp.mew.common.rest.login;

import lombok.*;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ForcedOfflineRequest {

    private long userId;
    private long token;
    private int gatewaySid;

}
