package com.fagp.mew.common.rest.bo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 贯穿 整个游戏平台 session
 * -9 表示没有更改过该字段
 */
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MewSession {

    public long uid = -9;  // userID
    public long tk = -9;   // token
    public int dtp = -9;   // 设备类型
    public int gwsid = -9;  //网关ID
//    private int lot;   //登录状态 0 表示该Session 没有执行大厅登录过， 1表示已经执行大厅登录； 此时只能通过重连;

    public int loc = -9;    // 当前位置 大厅=0; 游戏里面 游戏ID
    public int lsid = -9;   // 大厅服务器ID
    public int gsid = -9;   // 游戏服务器ID
    public int onl = -9;    // -1 表示下线，  1 在线 OnlineType;   0 表示弱在线 该状态暂时不用; 初始化 是没有上线状态

    public int gid = -9;  // 0 表示大厅
    public int rid = -9;  // 0 表示大厅
    public int tid = -9;  // 桌子好
    public int sno = -9;  // 座位号

    //play info
    public int  pt = -9;   // 玩耍总次数
    public long mbn = -9;  // 背包最大金额总和
    public long bt = -9;   // 总押注
    public long pmt = -9;  // 总赔付
    public long mpn = -9;  // 最大赔付
    public int  mpm = -9;  // 最大赔付倍数
    public int srid = -9; //slots rollerId

    public long stm = -9;  // 开始时间 棋牌锁定 开始时间
    public int otm = -9;   // 过期时间 棋牌锁定 时长 miao

    public long ioid = -9;  // 大厅进出记录ID
    public long rioid = -9; // 房间游戏记录ID //易操作更新为0

    public void update(MewSession other){
        if ( -9 != other.tk) {this.tk = other.tk;}
        if ( -9 != other.gwsid) {this.gwsid = other.gwsid;}

        if ( -9 != other.loc) {this.loc = other.loc;}
        if ( -9 != other.lsid) {this.lsid = other.lsid;}
        if ( -9 != other.gsid) {this.gsid = other.gsid;}
        if (  -9 != other.onl) {this.onl = other.onl;}

        if ( -9 != other.gid) {this.gid = other.gid;}
        if ( -9 != other.rid) {this.rid = other.rid;}
        if ( -9 != other.tid) {this.tid = other.tid;}
        if ( -9 != other.sno) {this.sno = other.sno;}

        if ( -9 != other.pt) {this.pt = other.pt;}
        if ( -9 != other.mbn) {this.mbn = other.mbn;}
        if ( -9 != other.bt) {this.bt = other.bt;}
        if ( -9 != other.pmt) {this.pmt = other.pmt;}
        if ( -9 != other.mpn) {this.mpn = other.mpn;}
        if ( -9 != other.mpm) {this.mpm = other.mpm;}
        if (-9 != other.srid) {this.srid = other.srid;}

        if ( -9 != other.stm) {this.stm = other.stm;}
        if ( -9 != other.otm) {this.otm = other.otm;}

        if ( -9 != other.ioid) {this.ioid = other.ioid;}
        if ( -9 != other.rioid) {this.rioid = other.rioid;}
    }


//    public void exitGame(long token){
    public void exitGame(){
//        this.tk = token;
        this.loc = 0;    // 当前位置 大厅=0; 游戏里面 游戏ID
        this.gsid = -9;   // 游戏服务器ID

        this.gid = -9;  // 0 表示大厅
        this.rid = -9;  // 0 表示大厅
        this.tid = -9;  // 桌子好
        this.sno = -9;  // 座位号

        //play info
        this.pt = -9;   // 玩耍总次数
        this.mbn = -9;  // 背包最大金额总和
        this.bt = -9;   // 总押注
        this.pmt = -9;  // 总赔付
        this.mpn = -9;  // 最大赔付
        this.mpm = -9;  // 最大赔付倍数
        this.srid = -9;

        this.stm = -9;  // 开始时间 棋牌锁定 开始时间
        this.otm = -9;   // 过期时间 棋牌锁定 时长 miao

        this.ioid = -9;  // 大厅进出记录ID
        this.rioid = -9; // 房间游戏记录ID //易操作更新为0
    }

    public void initPlayInfo(){
        this.pt = 0;   // 玩耍总次数
        this.mbn = 0;  // 背包最大金额总和
        this.bt = 0;   // 总押注
        this.pmt = 0;  // 总赔付
        this.mpn = 0;  // 最大赔付
        this.mpm = 0;  // 最大赔付倍数
        this.srid = 0;
    }

    public void incrementPayment(long goldNumber, long betNumber, long paymentNumber, int multiple, int rollerId){
        if (goldNumber > this.mbn){
            this.mbn = goldNumber;
        }
        this.bt += betNumber;
        this.pmt += paymentNumber;
        if (paymentNumber > this.mpn){
            this.mpn = paymentNumber;
        }
        if (multiple > this.mpm){
            this.mpm = multiple;
        }
        this.srid = rollerId;
        this.pt += 1;
        this.stm = -9;
        this.otm = -9;
    }
    public void chessBetLock(int overTime){
        this.stm = System.currentTimeMillis();
        this.otm = overTime;
    }

    /** true playing, false: not */
    public boolean checkChessPlaying(){
        if (this.otm == -9 || this.stm == -9){
            return false;
        }
        if (remainTime() < 1){
            return false;
        }
        return true;
    }


    public int remainingLock(){
        if (!checkChessPlaying()){
            return 0;
        }
       return remainTime();
    }

    private int remainTime(){
        long currentTime = System.currentTimeMillis();
        int remain = (int) ((currentTime - this.stm)/1000);
        return this.otm - remain;
    }

    public void setLockTime(long startTime, int otm){
        this.stm = startTime;
        this.otm = otm;
    }



}
